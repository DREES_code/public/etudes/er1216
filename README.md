# ER1216

Pour huit français sur dix, profiter le plus longtemps possible de la retraite reste la principale motivation de départ

Ce dossier contient le programme ayant servi à produire l'Etudes et résultats n°1216 "Pour huit français sur dix, profiter le plus longtemps possible de la retraite reste la principale motivation de départ". La fin du programme permet de reproduire les données complémentaires de l'étude, ainsi que quelques données annexes.

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données: DREES, COR, DSS, CNAV, Agirc-Arrco, SRE, CDC, CPRPSNCF, enquêtes Motivations de départ à la retraite 2014, 2017 et 2021.
